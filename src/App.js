import React, { useEffect, useContext } from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate, useLocation, useNavigate } from 'react-router-dom';
import { Box, CssBaseline, AppBar, Toolbar, Typography, Button } from '@mui/material';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import Register from './components/Register';
import Login from './components/Login';
import Home from './components/Home';
import Playlists from './components/Playlists';
import Videos from './components/Videos';
import ProfileSelection from './components/ProfileSelection';
import ProfileVideos from './components/ProfileVideos'; // Importar el nuevo componente
import { AuthProvider, AuthContext } from './context/AuthContext';
import Logo from './assets/Logo.png';
import ProfileManagement from './components/ProfileManagement';

const theme = createTheme({
  palette: {
    primary: {
      main: '#ff0000',
    },
    background: {
      default: '#000000',
    },
    text: {
      primary: '#ffffff',
    },
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <AuthProvider>
        <Router>
          <CssBaseline />
          <Header />
          <Box component="main" sx={{ flexGrow: 1, p: 3, mt: 8 }}>
            <Routes>
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route path="/profile-selection" element={<ProfileSelection />} />
              <Route path="/" element={<PrivateLayout><ProfileVideos /></PrivateLayout>} /> {/* Usar ProfileVideos en el home */}
              <Route path="/videos" element={<PrivateLayout><Videos /></PrivateLayout>} />
              <Route path="/playlists" element={<PrivateLayout><Playlists /></PrivateLayout>} />
              <Route path="/profiles" element={<PrivateLayout><ProfileManagement/></PrivateLayout>} />
              <Route path="*" element={<Navigate to="/login" />} />
            </Routes>
          </Box>
        </Router>
      </AuthProvider>
    </ThemeProvider>
  );
}

function Header() {
  const location = useLocation();
  const isAuthPage = location.pathname === '/login' || location.pathname === '/register' || location.pathname === '/profile-selection';
  const navigate = useNavigate();

  if (isAuthPage) return null;

  const profile = JSON.parse(localStorage.getItem('profile'));

  return (
    <AppBar position="fixed" sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}>
      <Toolbar>
        <img src={Logo} alt="KidsTube" style={{ height: 30, cursor: 'pointer' }} onClick={() => navigate('/')} />
        <Typography variant="h6" noWrap component="div" sx={{ flexGrow: 1, ml: 2 }}>
          KidsTube
        </Typography>
        {profile.age >= 18 && (
          <>
            <Button color="inherit" onClick={() => navigate('/videos')}>VIDEOS</Button>
            <Button color="inherit" onClick={() => navigate('/playlists')}>PLAYLISTS</Button>
            <Button color="inherit" onClick={() => navigate('/profiles')}>PROFILES</Button>
          </>
        )}
        <LogoutButton />
      </Toolbar>
    </AppBar>
  );
}

function PrivateLayout({ children }) {
  const { user } = useContext(AuthContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (!user) {
      navigate('/login');
    } else if (!localStorage.getItem('profile')) {
      navigate('/profile-selection');
    }
  }, [user, navigate]);

  return user ? <>{children}</> : <Navigate to="/login" />;
}

function LogoutButton() {
  const { logout } = useContext(AuthContext);
  const navigate = useNavigate();

  const handleLogout = () => {
    logout();
    navigate('/login');
  };

  return (
    <Button color="inherit" onClick={handleLogout}>
      LOGOUT
    </Button>
  );
}

export default App;
