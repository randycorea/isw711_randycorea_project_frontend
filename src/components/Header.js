// Header.js
import React from 'react';
import { AppBar, Toolbar, Typography, Button, Box, IconButton, Menu, MenuItem, Avatar } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import VideoLibraryIcon from '@mui/icons-material/VideoLibrary';
import PlaylistPlayIcon from '@mui/icons-material/PlaylistPlay';
import GroupIcon from '@mui/icons-material/Group';
import AddIcon from '@mui/icons-material/Add';
import { useNavigate } from 'react-router-dom';
import Logo from '../assets/Logo.png';
import { AuthContext } from '../context/AuthContext';

function Header() {
    const { user, logout } = React.useContext(AuthContext);
    const navigate = useNavigate();
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleLogout = () => {
        logout();
        navigate('/login');
    };

    return (
        <AppBar position="fixed" sx={{ backgroundColor: 'black' }}>
            <Toolbar>
                <img src={Logo} alt="KidsTube" style={{ height: 30, marginRight: 16, cursor: 'pointer' }} onClick={() => navigate('/')} />
                <Typography variant="h6" component="div" sx={{ flexGrow: 1, cursor: 'pointer' }} onClick={() => navigate('/')}>
                    KidsTube
                </Typography>
                {user && (
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                        <Button color="inherit" onClick={() => navigate('/videos')} startIcon={<VideoLibraryIcon />}>
                            Videos
                        </Button>
                        <Button color="inherit" onClick={() => navigate('/playlists')} startIcon={<PlaylistPlayIcon />}>
                            Playlists
                        </Button>
                        <Button color="inherit" onClick={() => navigate('/add-profile')} startIcon={<AddIcon />}>
                            Add Profile
                        </Button>
                        <Button color="inherit" onClick={() => navigate('/restricted-users')} startIcon={<GroupIcon />}>
                            Restricted Users
                        </Button>
                        <IconButton
                            size="large"
                            edge="end"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleMenu}
                            color="inherit"
                        >
                            <Avatar alt={user.firstName} src="/static/images/avatar/1.jpg" />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorEl}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={Boolean(anchorEl)}
                            onClose={handleClose}
                        >
                            <MenuItem onClick={handleLogout}>Logout</MenuItem>
                        </Menu>
                    </Box>
                )}
            </Toolbar>
        </AppBar>
    );
}

export default Header;
