import React, { useState, useContext } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import axios from 'axios';
import { Box, Button, TextField, Typography, Alert, Container, Paper, Snackbar } from '@mui/material';
import { AuthContext } from '../context/AuthContext';
import Logo from '../assets/Logo.png';

function Login() {
    const [formData, setFormData] = useState({ email: '', password: '' });
    const [error, setError] = useState(null);
    const [openSnackbar, setOpenSnackbar] = useState(false);
    const { login } = useContext(AuthContext);
    const navigate = useNavigate();

    const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setError(null);

        try {
            await login(formData.email, formData.password);
            setOpenSnackbar(true);
            setTimeout(() => {
                navigate('/profile-selection'); // Redirige a la selección de perfil
            }, 2000);
        } catch (err) {
            setError('Invalid Email or Password');
            setOpenSnackbar(true);
        }
    };

    const handleCloseSnackbar = () => {
        setOpenSnackbar(false);
    };

    return (
        <Container component="main" maxWidth="xs">
            <Paper elevation={6} sx={{ padding: 3, marginTop: 8, backgroundColor: 'rgba(0, 0, 0, 0.8)' }}>
                <Box sx={{ textAlign: 'center', marginBottom: 2 }}>
                    <img src={Logo} alt="KidsTube" style={{ height: 50 }} />
                </Box>
                <Box sx={{ textAlign: 'center', marginBottom: 2 }}>
                    <Typography component="h1" variant="h5" sx={{ color: 'white' }}>
                        Sign In
                    </Typography>
                </Box>
                <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }}>
                    {error && <Alert severity="error">{error}</Alert>}
                    <TextField
                        fullWidth
                        margin="normal"
                        name="email"
                        label="Email"
                        type="email"
                        value={formData.email}
                        onChange={handleChange}
                        required
                        InputLabelProps={{ style: { color: 'white' } }}
                        sx={{
                            '& .MuiInputBase-input': {
                                color: 'white',
                            },
                            '& .MuiOutlinedInput-root': {
                                '& fieldset': {
                                    borderColor: 'white',
                                },
                                '&:hover fieldset': {
                                    borderColor: 'white',
                                },
                                '&.Mui-focused fieldset': {
                                    borderColor: 'white',
                                },
                            },
                        }}
                    />
                    <TextField
                        fullWidth
                        margin="normal"
                        name="password"
                        label="Password"
                        type="password"
                        value={formData.password}
                        onChange={handleChange}
                        required
                        InputLabelProps={{ style: { color: 'white' } }}
                        sx={{
                            '& .MuiInputBase-input': {
                                color: 'white',
                            },
                            '& .MuiOutlinedInput-root': {
                                '& fieldset': {
                                    borderColor: 'white',
                                },
                                '&:hover fieldset': {
                                    borderColor: 'white',
                                },
                                '&.Mui-focused fieldset': {
                                    borderColor: 'white',
                                },
                            },
                        }}
                    />
                    <Button fullWidth variant="contained" color="primary" type="submit" sx={{ mt: 2 }}>
                        Sign In
                    </Button>
                    <Box mt={2} textAlign="center">
                        <Typography variant="body2" sx={{ color: 'white' }}>
                            Don't have an account? <Link to="/register" style={{ color: 'blue' }}>Register</Link>
                        </Typography>
                    </Box>
                </Box>
            </Paper>
            <Snackbar open={openSnackbar} autoHideDuration={6000} onClose={handleCloseSnackbar}>
                <Alert onClose={handleCloseSnackbar} severity={error ? 'error' : 'success'} sx={{ width: '100%' }}>
                    {error || 'Logged In Successfully'}
                </Alert>
            </Snackbar>
        </Container>
    );
}

export default Login;
