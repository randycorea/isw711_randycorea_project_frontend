import React, { useState, useEffect, useContext } from 'react';
import { Box, Container, Typography, Grid, Card, CardContent, TextField, Button, Dialog, DialogTitle, DialogContent, DialogActions, FormControl, InputLabel, Select, MenuItem, Snackbar, Alert } from '@mui/material';
import axios from 'axios';
import { AuthContext } from '../context/AuthContext';

function Playlists() {
    const [playlists, setPlaylists] = useState([]);
    const [profiles, setProfiles] = useState([]);
    const [search, setSearch] = useState('');
    const [selectedPlaylist, setSelectedPlaylist] = useState(null);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [playlistData, setPlaylistData] = useState({ name: '', associatedProfiles: [] });
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');
    const [snackbarSeverity, setSnackbarSeverity] = useState('success');
    const { user } = useContext(AuthContext);

    useEffect(() => {
        const fetchPlaylists = async () => {
            try {
                const response = await axios.get('http://localhost:5000/api/playlists');
                setPlaylists(response.data);
            } catch (err) {
                console.error(err);
            }
        };
        fetchPlaylists();
    }, []);

    useEffect(() => {
        const fetchProfiles = async () => {
            try {
                const response = await axios.get(`http://localhost:5000/api/profiles/user/${user._id}`);
                setProfiles(response.data);
            } catch (err) {
                console.error(err);
            }
        };
        fetchProfiles();
    }, [user._id]);

    const handleSearchChange = (e) => {
        setSearch(e.target.value);
    };

    const handleDialogOpen = (playlist) => {
        if (playlist) {
            setSelectedPlaylist(playlist);
            setPlaylistData({
                name: playlist.name,
                associatedProfiles: playlist.associatedProfiles.map(profile => profile._id)
            });
        } else {
            setSelectedPlaylist(null);
            setPlaylistData({ name: '', associatedProfiles: [] });
        }
        setDialogOpen(true);
    };

    const handleDialogClose = () => {
        setDialogOpen(false);
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setPlaylistData((prevData) => ({ ...prevData, [name]: value }));
    };

    const handleSelectChange = (e) => {
        const { value } = e.target;
        setPlaylistData((prevData) => ({ ...prevData, associatedProfiles: value }));
    };

    const handleSave = async () => {
        try {
            const dataToSave = { ...playlistData, owner: user._id };
            if (selectedPlaylist) {
                await axios.put(`http://localhost:5000/api/playlists/${selectedPlaylist._id}`, dataToSave);
                setSnackbarMessage('Playlist updated successfully');
            } else {
                await axios.post('http://localhost:5000/api/playlists', dataToSave);
                setSnackbarMessage('Playlist created successfully');
            }
            setSnackbarSeverity('success');
            setSnackbarOpen(true);
            const response = await axios.get('http://localhost:5000/api/playlists');
            setPlaylists(response.data);
            handleDialogClose();
        } catch (err) {
            setSnackbarMessage('Error saving playlist');
            setSnackbarSeverity('error');
            setSnackbarOpen(true);
            console.error(err);
        }
    };

    const handleDelete = async (playlistId) => {
        try {
            await axios.delete(`http://localhost:5000/api/playlists/${playlistId}`);
            setSnackbarMessage('Playlist deleted successfully');
            setSnackbarSeverity('success');
            setSnackbarOpen(true);
            const response = await axios.get('http://localhost:5000/api/playlists');
            setPlaylists(response.data);
        } catch (err) {
            setSnackbarMessage('Error deleting playlist');
            setSnackbarSeverity('error');
            setSnackbarOpen(true);
            console.error(err);
        }
    };

    const filteredPlaylists = playlists.filter((playlist) =>
        playlist.name.toLowerCase().includes(search.toLowerCase())
    );

    const handleSnackbarClose = () => {
        setSnackbarOpen(false);
    };

    return (
        <Box sx={{ display: 'flex', minHeight: '100vh', backgroundColor: 'black' }}>
            <Container>
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        minHeight: '80vh',
                        backgroundColor: 'black',
                    }}
                >
                    <Typography variant="h4" sx={{ color: 'white', mb: 2 }}>
                        Playlists
                    </Typography>
                    <TextField
                        variant="outlined"
                        placeholder="Search playlists"
                        value={search}
                        onChange={handleSearchChange}
                        sx={{ 
                            backgroundColor: 'white', 
                            borderRadius: '4px', 
                            width: '100%', 
                            maxWidth: '500px', 
                            mb: 4 
                        }}
                        inputProps={{ style: { color: 'black' } }}
                    />
                    <Button variant="contained" color="primary" onClick={() => handleDialogOpen(null)}>
                        Add Playlist
                    </Button>
                    <Grid container spacing={2} sx={{ mt: 4 }}>
                        {filteredPlaylists.map((playlist) => (
                            <Grid item xs={12} sm={6} md={4} lg={3} key={playlist._id}>
                                <Card sx={{ 
                                    backgroundColor: '#212121', 
                                    color: 'white', 
                                    borderRadius: '8px',
                                    display: 'flex',
                                    flexDirection: 'column',
                                    height: '100%'
                                }}>
                                    <CardContent sx={{ flexGrow: 1 }}>
                                        <Typography gutterBottom variant="h5" component="div">
                                            {playlist.name}
                                        </Typography>
                                        <Typography variant="body2" color="white">
                                            {`Videos: ${playlist.videosCount}`}
                                        </Typography>
                                        <Typography variant="body2" color="white">
                                            {playlist.associatedProfiles.map(profile => profile.name).join(', ')}
                                        </Typography>
                                    </CardContent>
                                    <Box sx={{ display: 'flex', justifyContent: 'space-between', p: 2 }}>
                                        <Button variant="contained" color="secondary" onClick={() => handleDialogOpen(playlist)}>
                                            Edit
                                        </Button>
                                        <Button variant="contained" color="error" onClick={() => handleDelete(playlist._id)}>
                                            Delete
                                        </Button>
                                    </Box>
                                </Card>
                            </Grid>
                        ))}
                    </Grid>
                </Box>
                <Dialog open={dialogOpen} onClose={handleDialogClose}>
                    <DialogTitle>{selectedPlaylist ? 'Edit Playlist' : 'Add Playlist'}</DialogTitle>
                    <DialogContent>
                        <TextField
                            margin="dense"
                            name="name"
                            label="Name"
                            fullWidth
                            value={playlistData.name}
                            onChange={handleInputChange}
                            inputProps={{ style: { color: 'black' } }} // Cambia el color del texto ingresado
                        />
                        <FormControl fullWidth margin="normal">
                            <InputLabel style={{ color: 'black' }}>Associated Profiles</InputLabel>
                            <Select
                                multiple
                                name="associatedProfiles"
                                value={playlistData.associatedProfiles}
                                onChange={handleSelectChange}
                                style={{ color: 'black' }}
                                MenuProps={{
                                    PaperProps: {
                                        sx: {
                                            backgroundColor: 'white',
                                            color: 'black'
                                        }
                                    }
                                }}
                            >
                                {profiles.map((profile) => (
                                    <MenuItem key={profile._id} value={profile._id} style={{ color: 'black' }}>
                                        {profile.name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleDialogClose} color="secondary">
                            Cancel
                        </Button>
                        <Button onClick={handleSave} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
                <Snackbar open={snackbarOpen} autoHideDuration={6000} onClose={handleSnackbarClose}>
                    <Alert onClose={handleSnackbarClose} severity={snackbarSeverity} sx={{ width: '100%' }}>
                        {snackbarMessage}
                    </Alert>
                </Snackbar>
            </Container>
        </Box>
    );
}

export default Playlists;
