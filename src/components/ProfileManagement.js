import React, { useState, useEffect, useContext } from 'react';
import { Box, Container, Typography, Grid, Card, Avatar, Button, TextField, Dialog, DialogActions, DialogContent, DialogTitle, Snackbar, Alert } from '@mui/material';
import axios from 'axios';
import { AuthContext } from '../context/AuthContext';
import { useNavigate } from 'react-router-dom';

const avatars = [
    'https://upload.wikimedia.org/wikipedia/commons/0/0b/Netflix-avatar.png',
    'https://i.pinimg.com/originals/61/54/76/61547625e01d8daf941aae3ffb37f653.png',
    'https://i.pinimg.com/originals/1b/71/b8/1b71b85dd741ad27bffa5c834a7ed797.png'
];

function ProfileManagement() {
    const { user } = useContext(AuthContext);
    const [profiles, setProfiles] = useState([]);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [profileData, setProfileData] = useState({ name: '', pin: '', avatar: avatars[0], age: '' });
    const [selectedProfile, setSelectedProfile] = useState(null);
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');
    const navigate = useNavigate();

    useEffect(() => {
        const fetchProfiles = async () => {
            try {
                const response = await axios.get(`http://localhost:5000/api/profiles/user/${user._id}`);
                setProfiles(response.data);
            } catch (err) {
                console.error(err);
            }
        };
        fetchProfiles();
    }, [user._id]);

    const handleDialogOpen = (profile) => {
        if (profile) {
            setSelectedProfile(profile);
            setProfileData(profile);
        } else {
            setSelectedProfile(null);
            setProfileData({ name: '', pin: '', avatar: avatars[0], age: '' });
        }
        setDialogOpen(true);
    };

    const handleDialogClose = () => {
        setDialogOpen(false);
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setProfileData((prevData) => ({ ...prevData, [name]: value }));
    };

    const handleSaveProfile = async () => {
        try {
            if (selectedProfile) {
                await axios.put(`http://localhost:5000/api/profiles/${selectedProfile._id}`, profileData);
            } else {
                await axios.post('http://localhost:5000/api/profiles', { ...profileData, userId: user._id });
            }
            const response = await axios.get(`http://localhost:5000/api/profiles/user/${user._id}`);
            setProfiles(response.data);
            handleDialogClose();
            setSnackbarMessage('Profile saved successfully');
            setSnackbarOpen(true);
        } catch (err) {
            console.error('Error saving profile:', err);
            setSnackbarMessage('Error saving profile');
            setSnackbarOpen(true);
        }
    };

    const handleDeleteProfile = async (profileId) => {
        try {
            await axios.delete(`http://localhost:5000/api/profiles/${profileId}`);
            const response = await axios.get(`http://localhost:5000/api/profiles/user/${user._id}`);
            setProfiles(response.data);
            setSnackbarMessage('Profile deleted successfully');
            setSnackbarOpen(true);
        } catch (err) {
            console.error('Error deleting profile:', err);
            setSnackbarMessage('Error deleting profile');
            setSnackbarOpen(true);
        }
    };

    const handleCloseSnackbar = () => {
        setSnackbarOpen(false);
    };

    return (
        <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', minHeight: '100vh', backgroundColor: 'black', padding: 2 }}>
            <Container>
                <Typography variant="h4" sx={{ color: 'white', mb: 4, mt: 2 }}>Manage Restricted Users</Typography>
                <Grid container spacing={2} justifyContent="center">
                    {profiles.map((profile) => (
                        <Grid item xs={12} sm={6} md={4} key={profile._id}>
                            <Card sx={{ backgroundColor: '#1c1c1c', color: 'white', borderRadius: '10px', padding: 2 }}>
                                <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                    <Avatar alt={profile.name} src={profile.avatar} sx={{ width: 80, height: 80, mb: 2 }} />
                                    <Typography variant="h6" sx={{ mb: 1 }}>{profile.name}</Typography>
                                    <Box sx={{ display: 'flex', gap: 1 }}>
                                        <Button variant="contained" color="primary" onClick={() => handleDialogOpen(profile)}>Edit</Button>
                                        <Button variant="contained" color="error" onClick={() => handleDeleteProfile(profile._id)}>Delete</Button>
                                    </Box>
                                </Box>
                            </Card>
                        </Grid>
                    ))}
                    <Grid item xs={12} sm={6} md={4}>
                        <Card sx={{ backgroundColor: '#ff1744', color: 'white', borderRadius: '10px', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100%' }}>
                            <Button variant="contained" sx={{ backgroundColor: '#ff1744', color: 'white', fontWeight: 'bold' }} onClick={() => handleDialogOpen(null)}>ADD NEW USER</Button>
                        </Card>
                    </Grid>
                </Grid>
            </Container>
            <Dialog open={dialogOpen} onClose={handleDialogClose}>
                <DialogTitle>{selectedProfile ? 'Edit User' : 'Add User'}</DialogTitle>
                <DialogContent>
                    <TextField
                        margin="dense"
                        name="name"
                        label="Name"
                        fullWidth
                        value={profileData.name}
                        onChange={handleInputChange}
                        InputProps={{ style: { color: 'black' } }}
                        sx={{ mb: 2 }}
                    />
                    <TextField
                        margin="dense"
                        name="pin"
                        label="PIN (6 digits)"
                        type="password"
                        fullWidth
                        value={profileData.pin}
                        onChange={handleInputChange}
                        InputProps={{ style: { color: 'black' } }}
                        sx={{ mb: 2 }}
                    />
                    <TextField
                        margin="dense"
                        name="age"
                        label="Age"
                        fullWidth
                        value={profileData.age}
                        onChange={handleInputChange}
                        InputProps={{ style: { color: 'black' } }}
                        sx={{ mb: 2 }}
                    />
                    <Typography variant="body1" sx={{ mb: 2, textAlign: 'center' }}>Select Avatar:</Typography>
                    <Grid container spacing={2} justifyContent="center">
                        {avatars.map((avatar) => (
                            <Grid item key={avatar}>
                                <Avatar
                                    alt="avatar"
                                    src={avatar}
                                    sx={{ width: 60, height: 60, cursor: 'pointer', border: profileData.avatar === avatar ? '2px solid blue' : 'none' }}
                                    onClick={() => setProfileData((prevData) => ({ ...prevData, avatar }))}
                                />
                            </Grid>
                        ))}
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleDialogClose} sx={{ color: '#f50057' }}>Cancel</Button>
                    <Button onClick={handleSaveProfile} sx={{ color: '#f50057' }}>Save</Button>
                </DialogActions>
            </Dialog>
            <Snackbar
                open={snackbarOpen}
                autoHideDuration={6000}
                onClose={handleCloseSnackbar}
                anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
            >
                <Alert onClose={handleCloseSnackbar} severity={snackbarMessage.includes('Error') ? 'error' : 'success'} sx={{ width: '100%' }}>
                    {snackbarMessage}
                </Alert>
            </Snackbar>
        </Box>
    );
}

export default ProfileManagement;
