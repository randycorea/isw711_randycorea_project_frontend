import React, { useState, useEffect, useContext } from 'react';
import { Box, Container, Typography, Grid, Avatar, Button, Dialog, DialogTitle, DialogContent, DialogActions, Snackbar, Alert, TextField } from '@mui/material';
import axios from 'axios';
import { AuthContext } from '../context/AuthContext';
import { useNavigate } from 'react-router-dom';
import Logo from '../assets/Logo.png';

function ProfileSelection() {
    const [profiles, setProfiles] = useState([]);
    const [selectedProfile, setSelectedProfile] = useState(null);
    const [pin, setPin] = useState(new Array(6).fill(''));
    const [openSnackbar, setOpenSnackbar] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');
    const [snackbarSeverity, setSnackbarSeverity] = useState('success');
    const { user } = useContext(AuthContext);
    const navigate = useNavigate();

    useEffect(() => {
        const fetchProfiles = async () => {
            try {
                const response = await axios.get(`http://localhost:5000/api/profiles/user/${user._id}`);
                setProfiles(response.data);
            } catch (err) {
                console.error(err);
            }
        };
        fetchProfiles();
    }, [user._id]);

    const handleProfileSelect = (profile) => {
        setSelectedProfile(profile);
    };

    const handlePinChange = (e, index) => {
        if (isNaN(e.target.value)) return;
        const newPin = [...pin];
        newPin[index] = e.target.value;
        setPin(newPin);

        // Focus on next input
        if (e.target.value !== '' && index < 5) {
            document.getElementById(`pin-input-${index + 1}`).focus();
        } else if (e.target.value === '') {
            document.getElementById(`pin-input-${index - 1}`).focus();
        }
    };

    const handleLogin = async () => {
        try {
            const pinCode = pin.join('');
            const response = await axios.post('http://localhost:5000/api/profiles/verify-pin', {
                profileId: selectedProfile._id,
                pin: pinCode
            });
            if (response.status === 200) {
                localStorage.setItem('profile', JSON.stringify(selectedProfile));
                setSnackbarMessage('Logged In Successfully');
                setSnackbarSeverity('success');
                setOpenSnackbar(true);
                setTimeout(() => {
                    navigate('/');
                }, 2000);
            } else {
                throw new Error('Incorrect PIN');
            }
        } catch (error) {
            setSnackbarMessage('Incorrect PIN');
            setSnackbarSeverity('error');
            setOpenSnackbar(true);
            console.error('Error verifying PIN:', error);
        }
    };

    const handleCloseSnackbar = () => {
        setOpenSnackbar(false);
    };

    return (
        <Box sx={{ display: 'flex', minHeight: '100vh', backgroundColor: 'black', alignItems: 'center', justifyContent: 'center' }}>
            <Container sx={{ textAlign: 'center' }}>
                <Box sx={{ mb: 4 }}>
                    <img src={Logo} alt="KidsTube" style={{ height: 30 }} />
                </Box>
                <Typography variant="h4" sx={{ color: 'white', mb: 4 }}>
                    Select Your Profile
                </Typography>
                <Grid container spacing={2} justifyContent="center">
                    {profiles.map((profile) => (
                        <Grid item key={profile._id}>
                            <Box
                                sx={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                    cursor: 'pointer',
                                    transition: 'transform 0.3s',
                                    '&:hover': {
                                        transform: 'scale(1.05)',
                                    },
                                }}
                                onClick={() => handleProfileSelect(profile)}
                            >
                                <Avatar
                                    src={profile.avatar}
                                    sx={{ width: 100, height: 100, mb: 2, bgcolor: 'grey.800' }}
                                />
                                <Typography sx={{ color: 'white' }}>{profile.name}</Typography>
                            </Box>
                        </Grid>
                    ))}
                </Grid>
                <Dialog
                    open={Boolean(selectedProfile)}
                    onClose={() => setSelectedProfile(null)}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Enter PIN for {selectedProfile && selectedProfile.name}</DialogTitle>
                    <DialogContent>
                        <Box sx={{ display: 'flex', justifyContent: 'center', gap: 1 }}>
                            {pin.map((value, index) => (
                                <TextField
                                    key={index}
                                    id={`pin-input-${index}`}
                                    value={value}
                                    onChange={(e) => handlePinChange(e, index)}
                                    type="text"
                                    inputProps={{
                                        maxLength: 1,
                                        style: {
                                            width: '3rem',
                                            height: '3rem',
                                            fontSize: '2rem',
                                            textAlign: 'center',
                                            backgroundColor: 'white',
                                            color: 'black',
                                        },
                                        onFocus: (e) => e.target.select(),
                                        onKeyDown: (e) => {
                                            if (e.key === 'Backspace' && e.target.value === '' && index > 0) {
                                                document.getElementById(`pin-input-${index - 1}`).focus();
                                            }
                                        }
                                    }}
                                />
                            ))}
                        </Box>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => setSelectedProfile(null)} color="secondary">
                            Cancel
                        </Button>
                        <Button onClick={handleLogin} color="primary">
                            Login
                        </Button>
                    </DialogActions>
                </Dialog>
                <Snackbar open={openSnackbar} autoHideDuration={6000} onClose={handleCloseSnackbar}>
                    <Alert onClose={handleCloseSnackbar} severity={snackbarSeverity} sx={{ width: '100%' }}>
                        {snackbarMessage}
                    </Alert>
                </Snackbar>
            </Container>
        </Box>
    );
}

export default ProfileSelection;
