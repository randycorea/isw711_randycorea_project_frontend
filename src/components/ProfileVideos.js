import React, { useState, useEffect } from 'react';
import { Box, Container, Typography, Grid, Card, CardMedia, CardContent, TextField } from '@mui/material';
import axios from 'axios';
import ReactPlayer from 'react-player';
import { useNavigate } from 'react-router-dom';

function ProfileVideos() {
    const [videos, setVideos] = useState([]);
    const [search, setSearch] = useState('');
    const navigate = useNavigate();
    const profile = JSON.parse(localStorage.getItem('profile'));

    useEffect(() => {
        if (!profile) {
            navigate('/profile-selection');
            return;
        }

        const fetchVideos = async () => {
            try {
                const response = await axios.get('http://localhost:5000/api/videos', {
                    params: { profileId: profile._id }
                });
                console.log('Fetched Videos:', response.data);
                setVideos(response.data);
            } catch (err) {
                console.error(err);
            }
        };
        fetchVideos();
    }, [profile, navigate]);

    const filteredVideos = profile.age >= 18
        ? videos.filter(video => video.name.toLowerCase().includes(search.toLowerCase()))
        : videos;

    return (
        <Box sx={{ display: 'flex', minHeight: '100vh', backgroundColor: 'black' }}>
            <Container>
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        minHeight: '80vh',
                        backgroundColor: 'black',
                    }}
                >
                    <Typography variant="h4" sx={{ color: 'white', mb: 2 }}>
                        Welcome to KidsTube
                    </Typography>
                    <Typography variant="h6" sx={{ color: 'white', mb: 2 }}>
                        The best platform for safe and curated content for kids.
                    </Typography>
                    {profile.age >= 18 && (
                        <TextField
                            variant="outlined"
                            placeholder="Search videos"
                            value={search}
                            onChange={(e) => setSearch(e.target.value)}
                            sx={{ 
                                backgroundColor: 'white', 
                                borderRadius: '4px', 
                                width: '100%', 
                                maxWidth: '500px', 
                                mb: 4 
                            }}
                            inputProps={{ style: { color: 'black' } }} // Color del texto de entrada
                        />
                    )}
                    <Grid container spacing={2} sx={{ mt: 4 }}>
                        {filteredVideos.map((video) => (
                            <Grid item xs={12} sm={6} md={4} lg={3} key={video._id}>
                                <Card sx={{ 
                                    backgroundColor: '#212121', 
                                    color: 'white', 
                                    borderRadius: '8px',
                                    display: 'flex',
                                    flexDirection: 'column',
                                    height: '100%'
                                }}>
                                    <CardMedia
                                        component={ReactPlayer}
                                        url={video.url}
                                        controls
                                        width="100%"
                                        height="200px"
                                    />
                                    <CardContent sx={{ flexGrow: 1 }}>
                                        <Typography gutterBottom variant="h5" component="div">
                                            {video.name}
                                        </Typography>
                                        <Typography variant="body2" color="white">
                                            {video.description}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </Grid>
                        ))}
                    </Grid>
                </Box>
            </Container>
        </Box>
    );
}

export default ProfileVideos;
