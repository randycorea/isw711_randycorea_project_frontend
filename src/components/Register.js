import React, { useState } from 'react';
import axios from 'axios';
import { Box, Button, Container, CssBaseline, TextField, Typography, Avatar, Grid, Paper, Snackbar, Alert } from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { useNavigate } from 'react-router-dom';
import Logo from '../assets/Logo.png';

function Register() {
  const [form, setForm] = useState({
    email: '',
    password: '',
    confirmPassword: '',
    phone: '',
    pin: '',
    name: '',
    firstName: '',
    lastName: '',
    country: '',
    birthDate: '',
  });
  const [openSnackbar, setOpenSnackbar] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [snackbarSeverity, setSnackbarSeverity] = useState('success');

  const navigate = useNavigate();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setForm((prevForm) => ({ ...prevForm, [name]: value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (form.password !== form.confirmPassword) {
      setSnackbarMessage("Passwords don't match!");
      setSnackbarSeverity('error');
      setOpenSnackbar(true);
      return;
    }
    const birthDate = new Date(form.birthDate);
    const age = new Date().getFullYear() - birthDate.getFullYear();
    if (age < 18) {
      setSnackbarMessage("You must be at least 18 years old to register.");
      setSnackbarSeverity('error');
      setOpenSnackbar(true);
      return;
    }
    try {
      const response = await axios.post('http://localhost:5000/api/users/register', form);
      console.log('User registered successfully:', response.data);
      setSnackbarMessage('User registered successfully');
      setSnackbarSeverity('success');
      setOpenSnackbar(true);
      setTimeout(() => {
        navigate('/login');
      }, 2000);
    } catch (error) {
      console.error('Error registering user:', error);
      setSnackbarMessage(`Error registering user: ${error.response?.data?.message || error.message}`);
      setSnackbarSeverity('error');
      setOpenSnackbar(true);
    }
  };

  const handleCloseSnackbar = () => {
    setOpenSnackbar(false);
  };

  return (
    <Container component="main" maxWidth="md">
      <CssBaseline />
      <Paper elevation={6} sx={{ padding: 2, backgroundColor: '#212121', borderRadius: 2 }}>
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <img src={Logo} alt="KidsTube" style={{ height: 60, marginBottom: 10 }} />
          <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  autoFocus
                  onChange={handleChange}
                  InputLabelProps={{ style: { color: '#ffffffb3' } }}
                  sx={{ backgroundColor: '#333', borderRadius: '4px', color: 'white' }}
                  inputProps={{ style: { color: 'white' } }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  onChange={handleChange}
                  InputLabelProps={{ style: { color: '#ffffffb3' } }}
                  sx={{ backgroundColor: '#333', borderRadius: '4px', color: 'white' }}
                  inputProps={{ style: { color: 'white' } }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="confirmPassword"
                  label="Confirm Password"
                  type="password"
                  id="confirmPassword"
                  autoComplete="current-password"
                  onChange={handleChange}
                  InputLabelProps={{ style: { color: '#ffffffb3' } }}
                  sx={{ backgroundColor: '#333', borderRadius: '4px', color: 'white' }}
                  inputProps={{ style: { color: 'white' } }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="phone"
                  label="Phone Number"
                  type="text"
                  id="phone"
                  onChange={handleChange}
                  InputLabelProps={{ style: { color: '#ffffffb3' } }}
                  sx={{ backgroundColor: '#333', borderRadius: '4px', color: 'white' }}
                  inputProps={{ style: { color: 'white' } }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="pin"
                  label="PIN (6 digits)"
                  type="password"
                  id="pin"
                  onChange={handleChange}
                  InputLabelProps={{ style: { color: '#ffffffb3' } }}
                  sx={{ backgroundColor: '#333', borderRadius: '4px', color: 'white' }}
                  inputProps={{ style: { color: 'white' } }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="name"
                  label="Profile Name"
                  type="text"
                  id="name"
                  onChange={handleChange}
                  InputLabelProps={{ style: { color: '#ffffffb3' } }}
                  sx={{ backgroundColor: '#333', borderRadius: '4px', color: 'white' }}
                  inputProps={{ style: { color: 'white' } }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="firstName"
                  label="First Name"
                  type="text"
                  id="firstName"
                  onChange={handleChange}
                  InputLabelProps={{ style: { color: '#ffffffb3' } }}
                  sx={{ backgroundColor: '#333', borderRadius: '4px', color: 'white' }}
                  inputProps={{ style: { color: 'white' } }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="lastName"
                  label="Last Name"
                  type="text"
                  id="lastName"
                  onChange={handleChange}
                  InputLabelProps={{ style: { color: '#ffffffb3' } }}
                  sx={{ backgroundColor: '#333', borderRadius: '4px', color: 'white' }}
                  inputProps={{ style: { color: 'white' } }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  margin="normal"
                  fullWidth
                  name="country"
                  label="Country"
                  type="text"
                  id="country"
                  onChange={handleChange}
                  InputLabelProps={{ style: { color: '#ffffffb3' } }}
                  sx={{ backgroundColor: '#333', borderRadius: '4px', color: 'white' }}
                  inputProps={{ style: { color: 'white' } }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="birthDate"
                  label="Birth Date"
                  type="date"
                  id="birthDate"
                  InputLabelProps={{ shrink: true, style: { color: '#ffffffb3' } }}
                  onChange={handleChange}
                  sx={{ backgroundColor: '#333', borderRadius: '4px', color: 'white' }}
                  inputProps={{ style: { color: 'white' } }}
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2, bgcolor: 'primary.main', color: 'white' }}
            >
              Register
            </Button>
          </Box>
        </Box>
      </Paper>
      <Snackbar open={openSnackbar} autoHideDuration={6000} onClose={handleCloseSnackbar}>
        <Alert onClose={handleCloseSnackbar} severity={snackbarSeverity} sx={{ width: '100%' }}>
          {snackbarMessage}
        </Alert>
      </Snackbar>
    </Container>
  );
}

export default Register;
