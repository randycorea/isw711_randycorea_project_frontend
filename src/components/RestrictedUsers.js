import React, { useState, useEffect } from 'react';
import { Box, Container, Typography, Grid, Card, CardContent, TextField, Button, Dialog, DialogTitle, DialogContent, DialogActions } from '@mui/material';
import axios from 'axios';

function RestrictedUsers() {
    const [users, setUsers] = useState([]);
    const [search, setSearch] = useState('');
    const [selectedUser, setSelectedUser] = useState(null);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [userData, setUserData] = useState({ name: '', email: '' });

    useEffect(() => {
        const fetchUsers = async () => {
            try {
                const response = await axios.get('http://localhost:5000/api/restricted-users');
                setUsers(response.data);
            } catch (err) {
                console.error(err);
            }
        };
        fetchUsers();
    }, []);

    const handleSearchChange = (e) => {
        setSearch(e.target.value);
    };

    const handleDialogOpen = (user) => {
        if (user) {
            setSelectedUser(user);
            setUserData(user);
        } else {
            setSelectedUser(null);
            setUserData({ name: '', email: '' });
        }
        setDialogOpen(true);
    };

    const handleDialogClose = () => {
        setDialogOpen(false);
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setUserData((prevData) => ({ ...prevData, [name]: value }));
    };

    const handleSave = async () => {
        try {
            if (selectedUser) {
                await axios.put(`http://localhost:5000/api/restricted-users/${selectedUser._id}`, userData);
            } else {
                await axios.post('http://localhost:5000/api/restricted-users', userData);
            }
            const response = await axios.get('http://localhost:5000/api/restricted-users');
            setUsers(response.data);
            handleDialogClose();
        } catch (err) {
            console.error(err);
        }
    };

    const handleDelete = async (userId) => {
        try {
            await axios.delete(`http://localhost:5000/api/restricted-users/${userId}`);
            const response = await axios.get('http://localhost:5000/api/restricted-users');
            setUsers(response.data);
        } catch (err) {
            console.error(err);
        }
    };

    const filteredUsers = users.filter((user) =>
        user.name.toLowerCase().includes(search.toLowerCase())
    );

    return (
        <Box sx={{ display: 'flex', minHeight: '100vh', backgroundColor: 'black' }}>
            <Container>
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        minHeight: '80vh',
                        backgroundColor: 'black',
                    }}
                >
                    <Typography variant="h4" sx={{ color: 'white', mb: 2 }}>
                        Restricted Users
                    </Typography>
                    <TextField
                        variant="outlined"
                        placeholder="Search users"
                        value={search}
                        onChange={handleSearchChange}
                        sx={{ 
                            backgroundColor: 'white', 
                            borderRadius: '4px', 
                            width: '100%', 
                            maxWidth: '500px', 
                            mb: 4 
                        }}
                        inputProps={{ style: { color: 'black' } }}
                    />
                    <Button variant="contained" color="primary" onClick={() => handleDialogOpen(null)}>
                        Add User
                    </Button>
                    <Grid container spacing={2} sx={{ mt: 4 }}>
                        {filteredUsers.map((user) => (
                            <Grid item xs={12} sm={6} md={4} lg={3} key={user._id}>
                                <Card sx={{ 
                                    backgroundColor: '#212121', 
                                    color: 'white', 
                                    borderRadius: '8px',
                                    display: 'flex',
                                    flexDirection: 'column',
                                    height: '100%'
                                }}>
                                    <CardContent sx={{ flexGrow: 1 }}>
                                        <Typography gutterBottom variant="h5" component="div">
                                            {user.name}
                                        </Typography>
                                        <Typography variant="body2" color="white">
                                            {user.email}
                                        </Typography>
                                    </CardContent>
                                    <Box sx={{ display: 'flex', justifyContent: 'space-between', p: 2 }}>
                                        <Button variant="contained" color="secondary" onClick={() => handleDialogOpen(user)}>
                                            Edit
                                        </Button>
                                        <Button variant="contained" color="error" onClick={() => handleDelete(user._id)}>
                                            Delete
                                        </Button>
                                    </Box>
                                </Card>
                            </Grid>
                        ))}
                    </Grid>
                </Box>
                <Dialog open={dialogOpen} onClose={handleDialogClose}>
                    <DialogTitle>{selectedUser ? 'Edit User' : 'Add User'}</DialogTitle>
                    <DialogContent>
                        <TextField
                            margin="dense"
                            name="name"
                            label="Name"
                            fullWidth
                            value={userData.name}
                            onChange={handleInputChange}
                        />
                        <TextField
                            margin="dense"
                            name="email"
                            label="Email"
                            fullWidth
                            value={userData.email}
                            onChange={handleInputChange}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleDialogClose} color="secondary">
                            Cancel
                        </Button>
                        <Button onClick={handleSave} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </Container>
        </Box>
    );
}

export default RestrictedUsers;
