import React, { useState, useEffect } from 'react';
import { Box, Container, Typography, Grid, Card, CardMedia, CardContent, TextField, Button, Dialog, DialogTitle, DialogContent, DialogActions, MenuItem, Select, FormControl, InputLabel, Snackbar, Alert } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import ReactPlayer from 'react-player';
import axios from '../context/axiosConfig';  // Importa la configuración de Axios

function Videos() {
    const [videos, setVideos] = useState([]);
    const [playlists, setPlaylists] = useState([]);
    const [search, setSearch] = useState('');
    const [selectedVideo, setSelectedVideo] = useState(null);
    const [dialogOpen, setDialogOpen] = useState(false);
    const [videoData, setVideoData] = useState({ name: '', url: '', description: '', playlist: '' });
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');
    const [snackbarSeverity, setSnackbarSeverity] = useState('success');
    const navigate = useNavigate();

    useEffect(() => {
        const fetchVideos = async () => {
            try {
                const response = await axios.get('http://localhost:5000/api/videos');
                setVideos(response.data);
            } catch (err) {
                console.error(err);
            }
        };
        fetchVideos();
    }, []);

    useEffect(() => {
        const fetchPlaylists = async () => {
            try {
                const response = await axios.get('http://localhost:5000/api/playlists');
                setPlaylists(response.data);
            } catch (err) {
                console.error(err);
            }
        };
        fetchPlaylists();
    }, []);

    const handleSearchChange = (e) => {
        setSearch(e.target.value);
    };

    const handleDialogOpen = (video) => {
        if (video) {
            setSelectedVideo(video);
            setVideoData(video);
        } else {
            setSelectedVideo(null);
            setVideoData({ name: '', url: '', description: '', playlist: '' });
        }
        setDialogOpen(true);
    };

    const handleDialogClose = () => {
        setDialogOpen(false);
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setVideoData((prevData) => ({ ...prevData, [name]: value }));
    };

    const handleSave = async () => {
        try {
            if (selectedVideo) {
                await axios.put(`http://localhost:5000/api/videos/${selectedVideo._id}`, videoData);
                setSnackbarMessage('Video updated successfully!');
            } else {
                await axios.post('http://localhost:5000/api/videos', videoData);
                setSnackbarMessage('Video added successfully!');
            }
            setSnackbarSeverity('success');
            const response = await axios.get('http://localhost:5000/api/videos');
            setVideos(response.data);
            handleDialogClose();
        } catch (err) {
            setSnackbarMessage('An error occurred. Please try again.');
            setSnackbarSeverity('error');
            console.error(err);
        } finally {
            setSnackbarOpen(true);
        }
    };

    const handleDelete = async (videoId) => {
        try {
            await axios.delete(`http://localhost:5000/api/videos/${videoId}`);
            const response = await axios.get('http://localhost:5000/api/videos');
            setVideos(response.data);
            setSnackbarMessage('Video deleted successfully!');
            setSnackbarSeverity('success');
        } catch (err) {
            setSnackbarMessage('An error occurred. Please try again.');
            setSnackbarSeverity('error');
            console.error(err);
        } finally {
            setSnackbarOpen(true);
        }
    };

    const handleSnackbarClose = () => {
        setSnackbarOpen(false);
    };

    const filteredVideos = videos.filter((video) =>
        video.name.toLowerCase().includes(search.toLowerCase())
    );

    return (
        <Box sx={{ display: 'flex', minHeight: '100vh', backgroundColor: 'black' }}>
            <Container>
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        minHeight: '80vh',
                        backgroundColor: 'black',
                    }}
                >
                    <Typography variant="h4" sx={{ color: 'white', mb: 2 }}>
                        Videos
                    </Typography>
                    <TextField
                        variant="outlined"
                        placeholder="Search videos"
                        value={search}
                        onChange={handleSearchChange}
                        sx={{ 
                            backgroundColor: 'white', 
                            borderRadius: '4px', 
                            width: '100%', 
                            maxWidth: '500px', 
                            mb: 4 
                        }}
                        inputProps={{ style: { color: 'black' } }}
                    />
                    <Button variant="contained" color="primary" onClick={() => handleDialogOpen(null)}>
                        Add Video
                    </Button>
                    <Grid container spacing={2} sx={{ mt: 4 }}>
                        {filteredVideos.map((video) => (
                            <Grid item xs={12} sm={6} md={4} lg={3} key={video._id}>
                                <Card sx={{ 
                                    backgroundColor: '#212121', 
                                    color: 'white', 
                                    borderRadius: '8px',
                                    display: 'flex',
                                    flexDirection: 'column',
                                    height: '100%'
                                }}>
                                    <CardMedia
                                        component={ReactPlayer}
                                        url={video.url}
                                        controls
                                        width="100%"
                                        height="200px"
                                    />
                                    <CardContent sx={{ flexGrow: 1 }}>
                                        <Typography gutterBottom variant="h5" component="div">
                                            {video.name}
                                        </Typography>
                                        <Typography variant="body2" color="white">
                                            {video.description}
                                        </Typography>
                                    </CardContent>
                                    <Box sx={{ display: 'flex', justifyContent: 'space-between', p: 2 }}>
                                        <Button variant="contained" color="secondary" onClick={() => handleDialogOpen(video)}>
                                            Edit
                                        </Button>
                                        <Button variant="contained" color="error" onClick={() => handleDelete(video._id)}>
                                            Delete
                                        </Button>
                                    </Box>
                                </Card>
                            </Grid>
                        ))}
                    </Grid>
                </Box>
                <Dialog open={dialogOpen} onClose={handleDialogClose}>
                    <DialogTitle>{selectedVideo ? 'Edit Video' : 'Add Video'}</DialogTitle>
                    <DialogContent>
                        <TextField
                            margin="dense"
                            name="name"
                            label="Name"
                            fullWidth
                            value={videoData.name}
                            onChange={handleInputChange}
                            InputProps={{ style: { color: 'black' } }} // Cambia el color del texto aquí
                        />
                        <TextField
                            margin="dense"
                            name="url"
                            label="URL"
                            fullWidth
                            value={videoData.url}
                            onChange={handleInputChange}
                            InputProps={{ style: { color: 'black' } }} // Cambia el color del texto aquí
                        />
                        <TextField
                            margin="dense"
                            name="description"
                            label="Description"
                            fullWidth
                            value={videoData.description}
                            onChange={handleInputChange}
                            InputProps={{ style: { color: 'black' } }} // Cambia el color del texto aquí
                        />
                        <FormControl fullWidth margin="normal">
                            <InputLabel style={{ color: 'black' }}>Playlist</InputLabel>
                            <Select
                                name="playlist"
                                value={videoData.playlist}
                                onChange={handleInputChange}
                                style={{ color: 'black' }}
                            >
                                {playlists.map((playlist) => (
                                    <MenuItem key={playlist._id} value={playlist._id}>
                                        {playlist.name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleDialogClose} color="secondary">Cancel</Button>
                        <Button onClick={handleSave} color="primary">Save</Button>
                    </DialogActions>
                </Dialog>
                <Snackbar open={snackbarOpen} autoHideDuration={6000} onClose={handleSnackbarClose}>
                    <Alert onClose={handleSnackbarClose} severity={snackbarSeverity} sx={{ width: '100%' }}>
                        {snackbarMessage}
                    </Alert>
                </Snackbar>
            </Container>
        </Box>
    );
}

export default Videos;
